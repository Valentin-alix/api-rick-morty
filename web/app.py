import requests
import json
from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def home() -> str:
    return render_template("home.html")


@app.route("/character/<name>")
def get_character(name: str) -> str:
    request = requests.get(f"https://rickandmortyapi.com/api/character/?name={name}")
    request_json = json.loads(request.text)['results']
    return render_template("character.html", character_infos=request_json)


@app.route('/characters/<page>')
def get_name_characters(page: int) -> str:
    request = requests.get(f"https://rickandmortyapi.com/api/character?page={page}")
    request_json = json.loads(request.text)
    max_page = request_json['info']['pages']
    names = [character['name'] for character in request_json['results']]
    return render_template("characters.html", names=names, page=page, max_page=max_page)


@app.route('/episode/<num>')
def get_episode(num: int) -> str:
    request = requests.get(f"https://rickandmortyapi.com/api/episode/{num}")
    request_json = json.loads(request.text)
    return render_template("episode.html", episode_infos=request_json)


@app.route('/location/<num>')
def get_location(num: int) -> str:
    request = requests.get(f"https://rickandmortyapi.com/api/location/{num}")
    request_json = json.loads(request.text)
    return render_template("location.html", location_infos=request_json)


if __name__ == "__main__":
    app.run(debug=True)
