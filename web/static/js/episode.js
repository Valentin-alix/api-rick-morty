function go_to_episode(path_episode)
{
    const num_episode = path_episode.substring(path_episode.lastIndexOf('/') + 1)
    Redirect('/episode/' + num_episode)
}