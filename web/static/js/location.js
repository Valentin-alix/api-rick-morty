function go_to_location(path_location)
{
    const num_location = path_location.substring(path_location.lastIndexOf('/') + 1)
    Redirect('/location/' + num_location)
}